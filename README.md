Jukenix Audio
=============

Standalone and stateless means of reading and transcoding audio formats. 

Project Scope
-------------

Endeavours to provide:

- native read support for all audio formats supplied by [The Xiph.Org Foundation](http://xiph.org) - notably Vorbis, FLAC, Opus and Speex
- native read support for AIFF PCM and WAV PCM
- a base upon which dependent packages may provide support for further formats
- transcoder support for the above, and a base so this too can be extended
- fallback support for alien formats (e.g. mp3) by hooking in [SoX](http://sox.sourceforge.net) and [SoXI](http://sox.sourceforge.net/soxi.html)

TODO
----

- Create examples of and test Ogg FLAC, Opus, Speex and Ogg PCM
- Clean up Exceptions
- investigate LAME and native mp3 support

Author
------

Clancy Hood <c@lfy.be>

