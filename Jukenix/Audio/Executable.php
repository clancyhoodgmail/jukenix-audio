<?php

namespace Jukenix\Audio;

use Jukenix\Audio\Exception\ExecutableException;

abstract class Executable
{
	protected static $_executables = array(
		
		// Format fallbacks
		'\\Jukenix\\Audio\\Format\\SOXIWrapper' => '/usr/bin/soxi',
		
		// Transcoders
		'\\Jukenix\\Audio\\Encoder\\flac' => '/usr/bin/flac',
		'\\Jukenix\\Audio\\Encoder\\oggenc'  => '/usr/bin/oggenc',
		'\\Jukenix\\Audio\\Encoder\\sox'  => '/usr/bin/sox',
		
		// Taggers
		'\\Jukenix\\Audio\\Tagger\\vorbiscomment' => '/usr/bin/vorbiscomment',
		'\\Jukenix\\Audio\\Tagger\\metaflac' => '/usr/bin/metaflac',
	);
	
	public static function register_executable( $Executable, $path )
	{
		static::$_executables[$Executable] = $path;
	}
	
	public static function get_executable( $Executable )
	{
		if($Executable[0] != '\\')
			$Executable = "\\$Executable";
			
		return isset(static::$_executables[$Executable]) ? static::$_executables[$Executable] : false;
	}
	
	public static function executables( $Class = false )
	{		
		$executables = array();
		
		foreach(static::$_executables as $Executable => $path)
			if($path && (!$Class || is_subclass_of($Executable, $Class)))
				$executables[$Executable] = $path;
		
		return $executables;
	}

	public static function execute( $commands )
	{
		if(!$commands = trim( implode(' | ', (array) $commands) ))
			return false;
			
		@exec($commands, $output, $status_code);
		
		if( $status_code )
			throw new ExecutableException("The command exited with a status of $status_code", $commands, $output);
		
		return $output;
	}

}