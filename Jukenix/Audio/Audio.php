<?php

namespace Jukenix\Audio;

use Jukenix\Audio\Definition\AudioFile;
use JsonSerializable;
use Jukenix\Audio\Interfaces\Hashable;

use Jukenix\Audio\Exception\JukenixException;

class Audio implements JsonSerializable, Hashable
{
	const CBR = 0b00;
	const VBR = 0b01;
	const ABR = 0b11;
	
	const FALSE   = 0x0;
	const TRUE    = 0x1;
	const UNKNOWN = 0x2;
	
	protected static $_bootstrapped = false;
	
	/*
		Array of Format classes known by Jukenix Audio.
		This can be appended to via Audio::register_format() and they 
		are expanded into a tree during Audio::bootstrap() (though they 
		may also be registered late, after bootstrap is done).
	*/
	
	protected static $_registrable_formats = array(
		'\\Jukenix\\Audio\\Format\\FLAC',
		'\\Jukenix\\Audio\\Format\\AIFF\\PCM',
		'\\Jukenix\\Audio\\Format\\Ogg\\Vorbis',
		'\\Jukenix\\Audio\\Format\\Ogg\\FLAC',
		'\\Jukenix\\Audio\\Format\\Ogg\\PCM',
		'\\Jukenix\\Audio\\Format\\Ogg\\Opus'
	);
	
	/*
		A traversable tree of formats
	*/
	
	protected static $_format_tree = array();
	
	/*
		A flat Array of formats by ID
	*/
	
	protected static $_formats = array();

	protected $filepath;
	
	protected $_format_handle;

	protected $properties = array(
	
		'filename'     => null, // Filename. required
		'format'       => null, // Format::ID. required
		'bytes'        => null, // Filesize. required
	
		'channels'     => null, // required
		'sample_rate'  => null, // required
		'samples'      => null, // required
		'bit_depth'    => null, // absent on lossy formats but required elsewhere
		
		'bitrate_mode' => 0b00, // CBR, VBR or ABR
		'audio_bytes'  => null, // for calculating bitrate and compression ratio 
		'options'      => array(), // we may also expect a nominal bitrate, quality or other encoder options
		
		'hash'         => null // sum of the decoded audio as big-endian, signed raw pcm
	);
	
	protected $tags;

	public static function bootstrap()
	{
		if(!Audio::$_bootstrapped){
		
			foreach(Audio::$_registrable_formats as $AudioClass)
				Audio::_initialize_format( $AudioClass );
				
			Audio::$_bootstrapped = true;
		}
	}

	public static function register_format( $AudioClass )
	{
		Audio::$_registrable_formats[] = $AudioClass;
		
		if(Audio::$_bootstrapped)
			Audio::_initialize_format( $AudioClass );
	}
	
	public static function get_format( $ID )
	{
		return isset(Audio::$_formats[ $ID ]) ? Audio::$_formats[ $ID ] : false;
	}

	public static function identify( $file, &$AudioFile = null )
	{	
		Audio::bootstrap();
		$AudioFile = new AudioFile( $file );
		return Audio::_traverse_format_tree( $AudioFile, Audio::$_format_tree );
	}

	protected static function &_initialize_format( $AudioClass )
	{
		$null = null; // for our &recursion

		if($AudioClass[0] != '\\')
			$AudioClass = "\\$AudioClass";

		$parent_class = get_parent_class($AudioClass);
		
		if($parent_class == 'Jukenix\\Audio\\Definition\\Container'){

			if($AudioClass::EXEC_WRAPPER && !Executable::get_executable($AudioClass))
				return $null;

			$classes = &Audio::$_format_tree;
		
		} else {
		
			if(!is_subclass_of($AudioClass, 'Jukenix\\Audio\\Definition\\Container'))  // check to prevent recursion
				throw new \RuntimeException("Class is not a Container");
			
			$classes = &Audio::_initialize_format($parent_class);
			
			if(is_null($classes))
				return $classes;
		}
					
		if(!isset($classes[$AudioClass])){
				
			$classes[$AudioClass] = array(
				'defines' => @constant("$AudioClass::ID"),
				'formats' => array()
			);
		
			if($classes[$AudioClass]['defines'])
				Audio::$_formats[ $classes[$AudioClass]['defines'] ] = $AudioClass;
		}
		
		return $classes[$AudioClass]['formats'];
	}
	
	protected static function _traverse_format_tree( AudioFile $file, array $format_tree )
	{
		foreach($format_tree as $TestClass => $definition){
			
			if(!$TestClass::identify_resource($file))
				continue;
				
			if($definition['formats'] && ($AudioClass = Audio::_traverse_format_tree( $file, $definition['formats'] )))
				return $AudioClass;
				
			if($definition['defines'])
				return $TestClass;
		}
		
		return false;
	}
		
	
	/* 	Though Jukenix Audio is stateless, here we respect stateful extensions and assume 
		that Audio objects may want to be wholly or partially reconstructed.
	*/
	
	public function __construct( $filepath, array $properties = array(), array $tags = array() )
	{
		$this->filepath = $filepath;
		
		if(!isset($properties['filename']))
			$properties['filename'] = pathinfo($filepath, PATHINFO_FILENAME);
		
		foreach($properties as $key => $value)
			$this->properties[$key] = $value;

		if($tags)
			$this->tags = new Tags( $tags );
	}
	
	public function format_handle()
	{	
		if( ! $this->_format_handle ){
		
			if(empty($this->properties['format'])){
	
				if(!$AudioClass = Audio::identify( $this->filepath, $AudioFile ))
					throw new \RuntimeException("Can't identify that file");
					
				$this->_format_handle = new $AudioClass( $AudioFile );
				
			}else{
				
				$AudioClass = Audio::$_formats[ $this->properties['format'] ];
				$this->_format_handle = new $AudioClass( $this->filepath );
			}
		}
		
		return $this->_format_handle;
	}
	
	public function close_handle()
	{
		$this->_format_handle = null;
	}
	
	public function format_class()
	{
		$this->inspect();
		return Audio::$_formats[ $this->properties['format'] ];
	}
	
	public function is_compressed()
	{
		return constant($this->format_class().'::COMPRESSED'); 
	}
	
	public function is_lossless()
	{
		return constant($this->format_class().'::LOSSLESS');
	}

	public function is_taggable()
	{
		return constant($this->format_class().'::TAGGABLE');
	}

	public function mimetype()
	{
		return constant($this->format_class().'::MIMETYPE');		
	}
	
	public function inspect( $force = false )
	{	
		if( empty($this->properties['format']) || $force )
			foreach($this->format_handle()->properties() as $key => $value)
				$this->properties[ $key ] = $value;
		
		return $this->properties;
	}	

	public function get_property( $key )
	{
		$this->inspect();
		return $this->properties[ $key ];		
	}

	public function tags()
	{	
		if(is_null($this->tags))
			$this->tags = new Tags( $this->format_handle()->tags() );

		return $this->tags;
	}
	
	public function get_filepath()
	{
		return $this->filepath;	
	}
	
	public function hash()
	{
		if(is_null($this->properties['hash'])){
		
			$this->inspect();
			$this->properties['hash'] = false;
			
			$Options = new Options( $this );
			
			$Options->set_options(array(
				'format' => 'pcm',
				'endianness' => false,
				'signed' => true,
	        	'channels' => $this->properties['channels'],
	        	'sample_rate' => $this->properties['sample_rate']				
			));
			
			try {
				
				$commands = Encoder::command_strings( $Options->command_path() );
				
				if(empty($commands)) // unlikely, since pcm is not a detectable format, but anyway
					$commands = array('cat '.escapeshellarg($this->filepath));
				
				$commands[] = 'sha1sum - 2>/dev/null';
				
				$output = Executable::execute($commands);
				
				$this->properties['hash'] = preg_replace('/[^a-z0-9]/', '', $output[0]);
				
			} catch( JukenixException $e ) {
			
				return false;
				
			}
		}
		
		return $this->properties['hash'];
	}

	public function jsonSerialize()
	{	
		return array(
			'properties' => $this->inspect(),
			'tags' => $this->tags()->jsonSerialize()
		);
	}
}

