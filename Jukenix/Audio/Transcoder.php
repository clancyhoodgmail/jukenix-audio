<?php

namespace Jukenix\Audio;

use Jukenix\Audio\Exception\TranscoderException;

abstract class Transcoder extends Executable
{

	public static function check_destination( $Audio, $output_format, $destination )
	{	
		if(empty($destination)){
		
			$appendix    = strpos($output_format, '.') ? "-$output_format" : ".$output_format";
			$destination = dirname($Audio->get_filepath()) .'/'. $Audio->get_property('filename') . $appendix;
		}
		
		return $destination;
	}
	
	public static function transcode( $Audio, $destination = null, array $options = array(), $GRACE = Options::GRACE_STRICT )
	{	
		if(is_string($Audio))
			$Audio = new Audio($Audio);
			
		$output_format = empty($options['format']) ? $Audio->get_property('format') : $options['format'];
		
		$destination = Transcoder::check_destination( $Audio, $output_format, $destination );
			
		$Options = new Options( $Audio, $GRACE );
		$Options->set_options( $options );
		
		if(!$commands = $Options->command_path( $destination )){
		
			if(!$destination)
				return $Audio->get_filepath();
					
			if(!@copy($Audio->get_filepath(), $destination))
				throw new TranscoderException("Failed to write to '$destination'");
				
			return $destination;			
		}
		
		Executable::execute( Encoder::command_strings( $commands ) );
	
		return $destination;
	}


}