<?php

namespace Jukenix\Audio\Encoder;

use Jukenix\Audio\Encoder;
use Jukenix\Audio\Transcoder;

/*
	flac: free lossless audio codec
	https://xiph.org/flac/documentation_tools_flac.html
*/

class flac extends Encoder{
	
	protected static $command_pattern = '{bin} {input-options} {source} {output-options} -o {destination}';

	protected static $formats = array(
		'flac'     => 0b1011, // P-IO
		'flac.oga' => 0b1011, // P-IO
		'pcm'      => 0b0011, // --IO
		'aiff'     => 0b0111, // -TIO
		'wav'      => 0b0011, // --IO
		'w64'      => 0b0111, // -TIO
		'rf64'     => 0b0011, // --IO
	);
	
	protected function pcm_section()
	{
		switch('pcm'){
		
			case $this->options['input_format']:
				return 'input-options';
				
			case $this->options['output_format']:
				return 'output-options';
		}
		
		throw new EncoderException("PCM options have been set, but PCM is not noe of my passed formats");
	}
	
	protected function set_option_signed( $bool )
	{
		$this->sections[ $this->pcm_section() ]['sign'] = '--sign ' . ($bool ? 'signed' : 'unsigned');
	}

	protected function set_option_little_endian( $bool )
	{
		$this->sections[ $this->pcm_section() ]['endian'] = '--endian ' . ($bool ? 'little' : 'big');
	}
	
	protected function set_option_channels( $int )
	{
		$this->sections[ $this->pcm_section() ]['channels'] = "--channels $int";
	}
	
	protected function set_option_sample_rate( $int )
	{
		$this->sections[ $this->pcm_section() ]['sample-rate'] = "--sample-rate $int";
	}
	
	protected function set_option_bit_depth( $int )
	{
		$this->sections[ $this->pcm_section() ]['bps'] = "--bps $int";
	}
	
	protected function set_option_input_format( $format )
	{		
		if(static::$formats[$format] & Encoder::FORMAT_PRIMARY)
			$this->sections['input-options'][] = '--decode';
	}
	
	protected function set_option_output_format( $format )
	{		
		switch($format){
		
			case 'pcm':
				$this->sections['output-options']['format'] = '--force-raw-format';
				break;
		
			case 'flac.oga':
				$this->sections['output-options']['format'] = '--ogg';
				break;
				
			case 'aiff':
				$this->sections['output-options']['format'] = '--force-aiff-format';
				break;
				
			case 'w64':
				$this->sections['output-options']['format'] = '--force-wave64-format';
				break;

			case 'rf64':
				$this->sections['output-options']['format'] = '--force-rf64-format';
				break;
		}
	}
}