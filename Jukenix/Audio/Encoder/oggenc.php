<?php

namespace Jukenix\Audio\Encoder;

use Jukenix\Audio\Encoder;

/*
	oggenc - encode audio into the Ogg Vorbis format *only* 
*/

class oggenc extends Encoder{
	
	protected static $command_pattern = '{bin} {options} {source} -o {destination}';

	protected static $formats = array(
		'oga'      => 0b1011, // P-IO
		
		'flac'     => 0b0010, // --I-
		'flac.oga' => 0b0010, // --I-
		'pcm'      => 0b0010, // --I-
		'aiff'     => 0b0010, // --I-
		'wav'      => 0b0010, // --I-
	);

	protected function fix_parent_options( Encoder $parent )
	{
		if($this->option('input_format') == 'pcm')
			$parent->override_option('signed', true);
	}

	protected static function set_option_input_format( $format )
	{		
		if($format == 'pcm')
			$this->sections['options'][] = '--raw';
	}

	protected function set_option_little_endian( $bool )
	{
		$this->sections['options']['raw-endianness'] = '--raw-endianness ' . ($bool ? 0 : 1);
	}
	
	protected function set_option_channels( $int )
	{
		$this->sections['options']['raw-chan'] = "--raw-chan $int";
	}
	
	protected function set_option_sample_rate( $int )
	{
		$this->sections['options']['raw-rate'] = "--raw-rate $int";
	}
	
	protected function set_option_bit_depth( $int )
	{
		$this->sections['options']['raw-bits'] = "--raw-bits $int";
	}
	
	protected function set_option_quality( $float )
	{
		$this->sections['options']['quality'] = "--quality $float";
	}
	
}