<?php

namespace Jukenix\Audio\Exception;

class ExecutableException extends \RuntimeException {

	function __construct( $message, $statis_code, $commands )
	{
		parent:: __construct( $message );
	}
}