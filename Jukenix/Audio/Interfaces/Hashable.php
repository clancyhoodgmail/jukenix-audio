<?php

namespace Jukenix\Audio\Interfaces;

interface Hashable
{
	public function hash();
}