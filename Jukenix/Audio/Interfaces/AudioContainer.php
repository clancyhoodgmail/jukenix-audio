<?php

namespace Jukenix\Audio\Interfaces;

use Jukenix\Audio\Definition\AudioFile;

interface AudioContainer
{
	public static function identify_resource( AudioFile $BIN );
}