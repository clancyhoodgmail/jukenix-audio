<?php

namespace Jukenix\Audio;

use Countable;
use JsonSerializable;
use Jukenix\Audio\Interfaces\Hashable;

class Tags implements Countable, JsonSerializable, Hashable
{
	protected $_tags = array();
	
	protected $_hash = null;
	
	public function __construct( array $tags )
	{
		foreach($tags as $key => $value)
			$this->set($key, $value);
	}

	public function import( Tags $Tags )
	{
		$this->_tags = $Tags->_tags;
		$this->_hash = $Tags->_hash;		
	}
	
	public function asArray()
	{
		ksort($this->_tags);
	
		foreach($this->_tags as &$tag)
			asort($tag);
	
		return $this->_tags;	
	}

	public function jsonSerialize()
	{
		return $this->asArray();
	}
    
    public function count()
    {
    	return count( $this->tags );
    }
	
	public function set( $key, $value )
	{
		if($this->_check_key($key))
			$this->_tags[ strtoupper($key) ] = $this->_sanitize_values( $value );
	}
	
	public function add( $key, $value )
	{
		$key = strtoupper($key);
	
		if($this->_check_key($key))
			$this->_tags[$key] = $this->_sanitize_values( @$this->_tags[$key], $value );		
	}
	
	public function get( $key )
	{
		$key = strtoupper($key);
		return isset($this->_tags[$key]) ? $this->_tags[$key] : null;
	}
	
	protected function _check_key( $key )
	{
		if(!$key || is_numeric($key)){ // string keys only
			trigger_error("Can't set empty or numeric tag names", E_USER_WARNING);
			return false;
		}
		
		$this->_hash = null;
		
		return true;
	}
	
	protected function _sanitize_values()
	{
		$array = array();
		
		foreach(func_get_args() as $value)
			foreach((array) $value as $string)
				$array[] = (string) $string;
			
		return array_unique( $array );
	}
	
	public function hash()
	{
		if(is_null($this->_hash))
			$this->_hash = empty($this->_tags) ? false : md5( json_encode( $this ) );
		
		return $this->_hash;
	}
}