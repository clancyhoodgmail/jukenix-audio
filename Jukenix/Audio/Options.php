<?php

namespace Jukenix\Audio;

use Jukenix\Audio\Interfaces\Hashable;
use Jukenix\Audio\Exception\OptionException;

class Options implements Hashable
{
	const GRACE_STRICT = 0x00;
	const GRACE_LAZY   = 0x01;
	
	const EXECUTE_NONE = 0x01;
	const EXECUTE_COPY = 0x02;
	
	protected $Audio;
	
	protected $GRACE;
	
	protected $parameters;
	
	protected $format;
	
	protected $options;
	
	protected $hash;
	
	public function __construct( Audio $Audio, $GRACE = Options::GRACE_STRICT )
	{
		$this->Audio = $Audio;
		$this->GRACE = $GRACE;
	}
	
	public function set_options( array $options )
	{
		$properties = $this->Audio->inspect();
		ksort($options);
		
		foreach(array(
			'bit_depth', 'channels', 'sample_rate' 
		) as $param){
			
			if(!empty($options[$param])){ // we going via pcm
				
				if($options[$param] != (int) $options[$param])
					throw new OptionException("Parameter $param must be an integer");
					
				if(($options[$param] > $properties[$param]) && ($GRACE == Options::GRACE_STRICT))
					throw new OptionException("Parameter $param cannot be a greater integer than source");
					
				else if($options[$param] < $properties[$param])
					$this->parameters[$param] = $options[$param];
			}
			
			unset($options[$param]);
		}
		
		if($this->parameters){ // normalize bool options
		
			$this->parameters['little_endian'] = (int)(bool) @$options['little_endian'];
			$this->parameters['signed'] = (int)(bool) @$options['signed'];
		}
		
		unset($options['little_endian'], $options['signed']);
		
		if(!empty($options['format']) && ($options['format'] != $properties['format'])) // unset for consistency and clarity (empty settings)
			$this->format = $options['format'];
			
		if(!$this->format && $this->parameters) // pcm is implied
			$this->format = 'pcm';
			
		unset($options['format']);
			
		if($this->parameters && ($this->format == 'pcm')) // there are no other options to be had
			$this->options = array();
			
		$this->options = $options; // anything remaining is an encoder option
		
		if( $this->parameters || $this->format || $this->options )
			$this->hash = md5(serialize(array( $this->parameters, $this->format, $this->options )));
	}
	
	public function hash()
	{
		return $this->hash;
	}

	public function command_path( $destination = false )
	{
		if( !$this->parameters && !$this->format && !$this->options )
			return array();

		$source = $this->Audio->get_filepath();
		$properties = $this->Audio->inspect();
		$parameters = (array) $this->parameters;
		$options = (array) $this->options;
		
		$input_format = $properties['format'];
		$output_format = $this->format ? $this->format : $properties['format'];
		
		$commands = array();
		
		if( $parameters ){ // raw pcm
		
			if(!$Decoder = Encoder::match_formats($input_format, 'pcm'))
				throw new OptionException("No decoders are available from format {$input_format}");
			
			$commands[] = new $Decoder($source, ($output_format == 'pcm') ? $destination : false, array(
				'input_format' => $input_format,
				'output_format' => 'pcm'
			) + $parameters );
			
			if($output_format == 'pcm') // we're done
				return $commands;
			
			$source = false;
			$input_format = 'pcm';
		}

		if($output_format != $input_format){
		
			if($Encoder = Encoder::match_formats($input_format, $output_format)){
			
				$commands[] = new $Encoder($source, $destination, array(
					'input_format'  => $input_format,
					'output_format' => $output_format
				) + $parameters + $options, end($commands) ); // params are either empty or needed
				
				return $commands;
			}
		}
		
		// now it gets more complicated. Find a TransitDecoder with a transit format the Encoder can agree with
		
		foreach(Encoder::collate_transcoders($input_format, Encoder::FORMAT_INPUT) as $TransitDecoder) {
		
			foreach($TransitDecoder::collate_formats(Encoder::FORMAT_TRANSIT) as $transit_format) {
				
				if(($transit_format != $input_format) && ($transit_format != $output_format)){
					
					if($Encoder = Encoder::match_formats($transit_format, $output_format)){
					
						$commands[] = new $TransitDecoder($source, false, array(
							'input_format' => $input_format,
							'output_format' => $transit_format
						) + $parameters, end($commands) ); // params are either empty or needed
						
						$commands[] = new $Encoder(false, $destination, array(
							'input_format' => $transit_format,
							'output_format' => $output_format
						) + $options, end($commands) );
						
						return $commands;					
					}
				}
			}
		}
						
		throw new OptionException("Failed to find a pathway between the formats");
	}

}