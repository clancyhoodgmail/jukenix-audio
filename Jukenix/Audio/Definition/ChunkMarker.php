<?php

namespace Jukenix\Audio\Definition;

class ChunkMarker
{
	public $offset = 0;

	public $size = 0;

	public $page_start = 0;

	function __construct( $offset = 0 )
	{
		$this->offset = $offset;
	}
	
	function set( $key, $value )
	{
		$this->$key = $value;
	}
	
	function set_many( $array )
	{
		foreach($array as $key => $value)
			$this->$key = $value;
	}

	function seek( AudioFile $BIN )
	{
		$BIN->seek( $this->offset );
	}

	function seek_page( AudioFile $BIN )
	{
		$BIN->seek( $this->offset + $this->page_start );
	}
	
	function seek_next( AudioFile $BIN )
	{
		$BIN->seek( $this->offset + $this->size );
	}
	
	function page_size()
	{
		return $this->size - $this->page_start;
	}

	function tell( AudioFile $BIN )
	{
		return $BIN->tell() - $this->offset - $this->page_start;
	}
	
	function remaining( AudioFile $BIN )
	{
		return $this->size - $this->tell( $BIN );
	}

	function eof( AudioFile $BIN )
	{
		return $BIN->tell() >= ($this->offset + $this->size);
	}
}