<?php

namespace Jukenix\Audio\Definition;

use Jukenix\Audio\Exception\AccessException;
use Jukenix\Audio\Exception\FormatException;

abstract class Container extends AudioFile
{

	const EXEC_WRAPPER = false;
	const TAGGABLE     = false;
		
	/*
		Container formats are varied, e.g. Ogg streams are true streams which don't know their length until 
		the last page. Hence we can't define much here, and implementing formats need to read their files
		in their own, most efficent way available.
	*/
	
	protected $_properties;
	
	protected $_tags;

	public function properties()
	{
		if(is_null($this->_properties))
		
			$this->_properties = array(
				'format' => $this::ID,
				'bytes' => $this->bytes(),
			) + $this->_read_properties();
		
		return $this->_properties;
	}

	public function tags()
	{
		if(is_null($this->_tags))
		
			$this->_tags = $this->_read_tags();
		
		return $this->_tags;
	}

	protected function _read_tags()
	{
		return array();
	}
}