<?php

namespace Jukenix\Audio\Definition;

use Jukenix\Audio\Exception\AccessException;

class AudioFile
{
	const BIG_ENDIAN    = 0x0000;
	const LITTLE_ENDIAN = 0x0001;
	
	protected $_filepath;
	protected $_open_mode;
	protected $_handle;
	protected $_requisitioned; // prevent file closing on __desctruct if we're being recycled
	
	protected $_bytes;
	
	function __construct( $filepath, $open_mode = "r" )
	{	
		if((!$filepath instanceOf AudioFile) || ($open_mode != $filepath->_open_mode)){
		
			if(!$this->_handle = @fopen($filepath, $open_mode))
				throw new AccessException("Failed to open $filepath - IO or permissions error");			

			$this->_filepath = $filepath;
			$this->_open_mode = $open_mode;
		
		}else 
		
			$this->_requisition( $filepath );
	}
	
	protected function _requisition( AudioFile $file )
	{
		foreach($file as $key => $value)
			$this->$key = $value;
			
		$file->_requisitioned = true;
	}
	
	function __destruct()
	{
		if( ! $this->_requisitioned )
			fclose($this->_handle);
	}

    /**
     * Filesize 
     * @return int
     */	
	
	function bytes()
	{
		if(is_null($this->_bytes))
			$this->_bytes = filesize( $this->_filepath );
			
		return $this->_bytes;
	}

    /**
     * Aliases of the PHP file functions ftell, fseek and feof
     */
	
	function tell()
	{
		return ftell( $this->_handle );
	}

	function seek( $offset, $whence = \SEEK_SET )
	{
		return fseek($this->_handle, $offset, $whence);
	}

	function eof()
	{
		return feof( $this->_handle );
	}
	  
    /**
     * Binary-safe word to integer
     * After Markus Nix <mnix@docuverse.de>
     *
     * @param string binary word
     * @param bool $signed
     * @return int
     */
    public static function big2int( $word, $signed = false )
	{
		$int = 0;
		$len = strlen($word);
		
		for ($i = 0; $i < $len; $i++)
			$int += ord($word[$i]) * pow(256, ($len - 1 - $i));

        if ($signed && $int && $len < 5) {

			$mask = 0x80 << (($len - 1) * 8);
			
			if ($int & $mask)
				$int = 0 - ($int & ($mask - 1));
        }
		
		return (int) $int;
    }

    /**
     * Big-endian to binary string
     * After Markus Nix <mnix@docuverse.de>
     *
     * @param string binary word
     * @return string
     */   
    public static function big2bin( $word )
    {
        $bin = '';
        $len = strlen($word);

        for ($i = 0; $i < $len; $i++) 
            $bin .= str_pad(decbin(ord($word[$i])), 8, '0', STR_PAD_LEFT);

        return $bin;
    }
    
   	public static function decbin2float( $numerator )
    {
        return bindec( $numerator ) / bindec(str_repeat('1', strlen( $numerator )));
    }

	public static function big2float( $word )
    {
        // ANSI/IEEE Standard 754-1985, Standard for Binary Floating Point Arithmetic
        // http://www.psc.edu/general/software/packages/ieee/ieee.html
        // http://www.scri.fsu.edu/~jac/MAD3401/Backgrnd/ieee.html

        $bitword = static::big2bin( $word );
        $signbit = $bitword[0];

		switch (strlen( $word ) * 8) {

			case 80:
				// 80-bit Apple SANE format
				// http://www.mactech.com/articles/mactech/Vol.06/06.01/SANENormalized/
				$exponentstring = substr($bitword, 1, 15);
				$isnormalized = intval($bitword[16]);
				$fractionstring = substr($bitword, 17, 63);
				$exponent = pow(2, bindec($exponentstring) - 16383);
				$fraction = $isnormalized + static::decbin2float($fractionstring);
				$float_val = $exponent * $fraction;
				
				return (float) ($signbit ? $float_val *-1 : $float_val);

			case 32:
				$exponentbits = 8;
				$fractionbits = 23;
				break;

			case 64:
				$exponentbits = 11;
				$fractionbits = 52;
				break;

			default:
				return false;
		}

        $exponentstring = substr($bitword, 1, $exponentbits);
        $fractionstring = substr($bitword, 9, $fractionbits);

        $exponent = bindec($exponentstring);
        $fraction = bindec($fractionstring);

        if($exponent == (pow(2, $exponentbits) - 1)){
        
        	if($fraction != 0) // Not a number
        		return false;
        		
        	return ($signbit == '1') ? '-infinity' : '+infinity';
		}

		if ($exponent == 0){
			
			if($fraction == 0)
				return (float) ($signbit ? -0 : 0);
				
			// These are 'unnormalized' values
			$float_val = pow(2, (-1 * (pow(2, $exponentbits - 1) - 2))) * static::decbin2float($fractionstring);
	
			return (float) ($signbit ? $float_val *-1 : $float_val);
		}
 			
		$float_val = pow(2, ($exponent - (pow(2, $exponentbits - 1) - 1))) * (1 + static::decbin2float($fractionstring));
		
		return (float) ($signbit ? $float_val *-1 : $float_val);
    }
    
    /**
     * Read $len bytes from disk in $ness byte order
     *
     * @param int $len length bytes
     * @param int $ness const endianness
     * @return string binary string
     *
     */
	function read( $len, $ness = AudioFile::BIG_ENDIAN )
	{
		return ($ness == AudioFile::BIG_ENDIAN) ? fread($this->_handle, $len) : strrev( fread($this->_handle, $len) );
	}

    /**
     * Read $len bytes from disk in $ness byte order and return as decimal
     *
     * @param int $len length bytes
     * @param int $ness const endianness
     * @return mixed int or float from bindec
     *
     */
	function decread( $len, $ness = AudioFile::BIG_ENDIAN )
	{
		return bindec($this->read($len, $ness));
	}

    /**
     * Read $len bytes from disk in $ness byte order and return as int
     *
     * @param int $len length bytes
     * @param int $ness const endianness
     * @param bool $signed 
     * @return int
     *
     */
	function intread( $len, $ness = AudioFile::BIG_ENDIAN, $signed = false )
	{
		return static::big2int($this->read($len, $ness), $signed);
	}

    /**
     * Read a big-endian word and return as integer
     *
     * @param int $len length bytes
     * @param bool $signed 
     * @return int
     *
     */	
	function bigread( $len, $signed = false )
	{
		return $this->intread( $len, AudioFile::BIG_ENDIAN, $signed );
	}
	
    /**
     * Read a little-endian word and return as integer
     *
     * @param int $len length bytes
     * @param bool $signed 
     * @return int
     *
     */
	function litread( $len, $signed = false  )
	{
		return $this->intread( $len, AudioFile::LITTLE_ENDIAN, $signed );
	}

    /**
     * Read an array in the form $name => $len and return $name => $byte
     *
     * @param array $array the input array
     * @param int $ness const endianness
     * @return array
     *
     */
	function assign( $array, $ness = AudioFile::BIG_ENDIAN )
	{
		foreach($array as $key => $len)
			$array[$key] = $this->intread($len, $ness);
		
		return $array;
	}
	
}