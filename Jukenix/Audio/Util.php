<?php

namespace Jukenix\Audio;

abstract class Util
{

	public static function humanise($int, $precision = 2, $suffix = 'b', $m = 1024)
	{	
		if($m == 1024){
			switch($suffix){
				case 'b':
				case 'B':
					$suffix = "i$suffix";
			}
		}
		
		$unim = array($b,"k$suffix","M$suffix","G$suffix","T$suffix","P$suffix");
		$c = 0;
		while ($int >= $m) {
		    $c++;
		    $int = $int/$m;
		}
		
		return number_format($int, ($c ? $precision : 0))." ".$unim[$c];
	}
	
	public static function dehumanise($string, $m = 1024)
	{
		$unim = array(false, 'K', 'M', 'G', 'T', 'P');
		$float = floatval($string);
		$name = substr(preg_replace('/[^A-Z]/', '', strtoupper($string)), 0, 1);
		
		for($c = 0; ($c < 6) && ($name != $unim[$c]); $c++)
			$float *= $m;
			
		return ($c < 6) ? $float : null;
	}	
}