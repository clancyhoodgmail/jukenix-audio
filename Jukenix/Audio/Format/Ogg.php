<?php

namespace Jukenix\Audio\Format;

use Jukenix\Audio\Interfaces\AudioContainer;

use Jukenix\Audio\Audio;
use Jukenix\Audio\Definition\AudioFile;
use Jukenix\Audio\Definition\Container;
use Jukenix\Audio\Definition\ChunkMarker;
use Jukenix\Audio\Exception\FormatException;

abstract class Ogg extends Container implements AudioContainer
{
	// anything audio in an ogg has the mimetype audio/ogg
	// http://wiki.xiph.org/MIME_Types_and_File_Extensions
	
	const MIMETYPE = 'audio/ogg';
	const TAGGABLE = true;
	
	protected $_ogg_data = array(
		'bos' => null,
		'eos' => null,
		'comment_strings' => array(),
		'non_audio_bytes' => 0
	);
	
	public static function identify_resource( AudioFile $BIN )
	{
		$BIN->seek(0);
		return ($BIN->read(4) == 'OggS');
	}
	
	
	protected static function _extract_format_id( AudioFile $BIN )
	{
		$BIN->seek(26);
		$page_segments = $BIN->litread(1);
		$BIN->seek($page_segments, SEEK_CUR);
		return preg_replace('/[^a-z]/', '', strtolower($BIN->read(8)));
	}

	protected function _next_chunk( $chunk = false )
	{

		/* Now *this* is documentation
		
		  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1| Byte
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | capture_pattern: Magic number for page start "OggS"           | 0-3
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | version       | header_type   | granule_position              | 4-7
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                                                               | 8-11
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                               | bitstream_serial_number       | 12-15
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                               | page_sequence_number          | 16-19
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                               | CRC_checksum                  | 20-23
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                               |page_segments  | segment_table | 24-27
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | ...                                                           | 28-
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 
		*/
	
		if($chunk)
			$chunk->seek_next($this);
	
		if($this->read(4) != 'OggS')
			throw new FormatException("Lost my page! on OGG {$this->_filepath}");
		
		$chunk = new ChunkMarker($this->tell() - 4);

		$chunk->set('version',     $this->litread(1));
		$chunk->set('header_type', $this->litread(1));
		
		$chunk->set('fresh', $chunk->header_type & 0x01); 
		$chunk->set('bos',   $chunk->header_type & 0x02); 
		$chunk->set('eos',   $chunk->header_type & 0x04); 

		$chunk->set('granule_position', $this->litread(8));
		
		// bitstream_serial_no 4
		// page_sequence_number 4
		// crc_checksum 4
		$this->seek(12, \SEEK_CUR);
		
		$page_segments = $this->litread(1);
		$page_size = 0;
		
		for($n=0; $n < $page_segments; $n++)
			$page_size += $this->litread(1);
			
		$chunk->set('page_start', $this->tell() - $chunk->offset);
		$chunk->set('size', $page_size + $chunk->page_start);

		return $chunk;
	}
	
	/*
		An Ogg is a true stream, so we have to read the whole thing to collect all our properties (notably n samples). 
		Scan through all the non-audio pages and save what we need for later
	*/
	
	protected function _read_ogg()
	{
		if(is_null($this->_ogg_data['bos'])){
	
			$this->seek(0);
			
			// bos (beginning of stream) page
			$chunk = $this->_next_chunk();
			$this->_ogg_data['non_audio_bytes'] += $chunk->size;
			$this->_ogg_data['bos'] = $chunk;
			
			// comment page ?
			$chunk = $this->_next_chunk( $chunk );
			$this->_ogg_data['non_audio_bytes'] += $chunk->size;
			
			if($this->_identify_comment_page()){ // throw exception where illegal to omit comment page 
	
				/*
				1) [vendor_length] = read an unsigned integer of 32 bits 
				2) [vendor_string] = read a UTF-8 vector as [vendor_length] octets 
				3) [user_comment_list_length] = read an unsigned integer of 32 bits 
				4) iterate [user_comment_list_length] times { 
					5) [length] = read an unsigned integer of 32 bits 
					6) this iteration's user comment = read a UTF-8 vector as [length] octets.
						comment strings overflow into following pages, so check how far you are and skip into next page if necessary
				} 
				7) [framing_bit] = read a single bit as boolean 
				8) if ( [framing_bit] unset or end-of-packet ) then ERROR 
				9) done.
				*/
					
				$vendor_length = $this->litread(4);
				$comments_vendor = $this->read($vendor_length);
				$user_comment_list_length = $this->litread(4);
				
				for( $i=0; $i<$user_comment_list_length; $i++ ){
				
					$length = $this->litread(4);
					$comment = '';
					
					while($length){
					
						$to_read = min($length, $chunk->remaining($this));
						$comment .= $this->read($to_read);
						$length -= $to_read;
						
						if($chunk->eof( $this )){
							$chunk = $this->_next_chunk( $chunk );
							$this->_ogg_data['non_audio_bytes'] += $chunk->size;
						}
					}
					
					$this->_ogg_data['comment_strings'][] = $comment;
				}
			}
			
			while(!$chunk->eos)
				if((!$chunk = $this->_next_chunk($chunk)) || $this->eof())
					throw new FormatException("Can't find the end of stream");
			
			$this->_ogg_data['eos'] = $chunk;
		}
		
		return $this->_ogg_data;
	}
	
	protected function _identify_comment_page()
	{
		return false;
	}
	
	protected function _read_tags()
	{	
		$this->_read_ogg();
		$comments = array();
		
		foreach($this->_ogg_data['comment_strings'] as $string){
			list($key, $val) = explode('=', $string, 2);
			$comments[$key][] = $val;			
		}
		
		return $comments;
	}

}