<?php

namespace Jukenix\Audio\Format;

use Jukenix\Audio\Interfaces\AudioContainer;
use Jukenix\Audio\Interfaces\AudioFormat;

use Jukenix\Audio\Audio;
use Jukenix\Audio\Definition\AudioFile;
use Jukenix\Audio\Definition\Container;
use Jukenix\Audio\Definition\ChunkMarker;

use Jukenix\Audio\Exception\FormatException;

class FLAC extends Container implements AudioContainer, AudioFormat
{

	const ID         = 'flac';
	const MIMETYPE   = 'audio/flac';
	const COMPRESSED = true;
	const LOSSLESS   = true;
	const TAGGABLE   = true;

	protected static $encoder_options = array(
		'compression_level' => 'int; between 0 and 8; default 5',
		'mid_side' => 'bool',
		'adaptive_mid_side' => 'bool',
		'keep_foreign_metadata' => 'bool'
	);

	const STREAMINFO     = 0;
	const PADDING        = 1;
	const APPLICATION    = 2;
	const SEEKTABLE      = 3;
	const VORBIS_COMMENT = 4;
	const CUESHEET       = 5;
	const PICTURE        = 6;
	
	protected $_chunks;
	
	public static function identify_resource( AudioFile $BIN )
	{
		$BIN->seek(0);
		return ($BIN->read(4) == 'fLaC');
	}		

	protected function _next_chunk( $chunk )
	{
		$chunk ? $chunk->seek_next($this) : $this->seek(4); // fLaC magic number appears at file start only
	
		$chunk = new ChunkMarker($this->tell());

		// always 4 bytes in, after 'fLaC' or last block flag + page size
		$chunk->set('page_start', 4);
		
		$byte = static::big2bin($this->read(1));
		
		$chunk->set('last_metadata_block', substr($byte, 0, 1));
		$chunk->set('id', bindec(substr($byte, 1)));
		$chunk->set('size', $chunk->page_start + bindec(static::big2bin($this->read(3)) ));
	
		return $chunk;
	}
	
	protected function _read_chunks()
	{
		if(is_null($this->_chunks)){
		
			$chunk = false;
			
			while(!$this->eof()){
				
				$chunk = $this->_next_chunk( $chunk );
				$this->_chunks[] = $chunk;
	
				// we don't care about audio blocks
				if($chunk->last_metadata_block)
					break;
			}
		}
		
		return $this->_chunks;
	}

	protected function _read_properties()
	{
		$chunks = $this->_read_chunks();
		
		$meta_bytes = 0;
		foreach($chunks as $chunk)
			$meta_bytes += $chunk->size;
	
		$streaminfo = $chunks[0];
		$streaminfo->seek_page( $this );
	
		$minblocksize = unpack('n', $this->read(2));
		$maxblocksize = unpack('n', $this->read(2));
		
		// minframesize 3
		// maxframesize 3
		
		$this->seek(6, \SEEK_CUR);
		
		$bin_str = static::big2bin( $this->read(8) );

		return array(
			'channels' => 1 + bindec( substr($bin_str, 20, 3) ),
			'sample_rate' => bindec( substr($bin_str, 0, 20) ),
			'samples' => bindec( substr($bin_str, 28, 36) ),
			'bit_depth' => 1 + bindec( substr($bin_str, 23, 5) ),
			'bitrate_mode' => ($minblocksize == $maxblocksize) ? Audio::CBR : Audio::VBR,
			'audio_bytes' => $this->bytes() - $meta_bytes
		);
	}
	
	protected function _read_tags()
	{
		$commment_strings = array();
		$chunks = $this->_read_chunks();
		
		while($chunk = current($chunks)){
			
			if($chunk->id == FLAC::VORBIS_COMMENT){
				
				$chunk->seek_page($this);
			
				$vendor_length = $this->litread(4);
				$comments_vendor = $this->read($vendor_length);
				$user_comment_list_length = $this->litread(4);
												
				for( $i=0; $i<$user_comment_list_length; $i++ ){
				
					// in case we hit a PADDING block in between comment pages 
					// (not sure is legal to be there, but just in case)
	
					while($chunk->id != FLAC::VORBIS_COMMENT)
						if(!$chunk = next($chunks))
							break 2;							
				
					$length = $this->litread(4);
					$comment = '';
					
					while( $length ){
					
						$to_read = min($length, $chunk->remaining($this));
						$comment .= $this->read($to_read);
						$length -= $to_read;
						
						if($chunk->eof( $this ))
							if(!$chunk = next($chunks))
								break 3;
					}
					
					$commment_strings[] = $comment;
				}
			}
			
			next($chunks);
		}
		
		$comments = array();
		
		foreach($commment_strings as $string){
		
			list($key, $val) = explode('=', $string, 2);
			$comments[$key][] = $val;			
		}
		
		return $comments;	
	}
	
}