<?php

namespace Jukenix\Audio\Format;

use Jukenix\Audio\Definition\AudioFile;
use Jukenix\Audio\Definition\Container;
use Jukenix\Audio\Interfaces\AudioContainer;

abstract class AIFF extends Container implements AudioContainer
{
	const TAGGABLE = true;

	public static function identify_resource( AudioFile $BIN )
	{
		$BIN->seek(0);
		return ($BIN->read(4) == 'FORM');
	}	
}