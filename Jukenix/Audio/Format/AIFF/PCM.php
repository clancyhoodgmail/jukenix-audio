<?php

namespace Jukenix\Audio\Format\AIFF;

use Jukenix\Audio\Format\AIFF;
use Jukenix\Audio\Interfaces\AudioFormat;
use Jukenix\Audio\Definition\AudioFile;
use Jukenix\Audio\Definition\ChunkMarker;
use Jukenix\Audio\Exception\FormatException;

class PCM extends AIFF implements AudioFormat
{

	const ID = 'aiff';
	const MIMETYPE = 'audio/aiff';
	const COMPRESSED = false;
	const LOSSLESS = true;
	
	protected $_chunks;
	
	protected $_audio_offset;

	public static function identify_resource( AudioFile $BIN )
	{		
		$BIN->seek(8);
		return ($BIN->read(4) == 'AIFF');
	}	

	/* Regular PCM AIFF
	
		key             n       example
	
		ckID            4       FORM
		ckSize          4       176516
		formType        4       AIFF
		
		Common Chunk
		
		ckID            4       COMM
		ckSize          4       18
		nChannels       2       2
		numSampleFrames 4       88200
		sampleSize      2       16
		sampleRate      10		44100.00 (Apple SANE format)
		
		
		( AIFC includes a 12-byte version chunk before the common chunk:
		http://www-mmsp.ece.mcgill.ca/documents/audioformats/AIFF/Docs/AIFF-C.9.26.91.pdf ) 
		
	*/


	protected function _read_chunks()
	{
		if(is_null($this->_chunks)){

			$this->seek(0);
			$chunk = new ChunkMarker(0);	
			$chunk->set('id', $this->read(4));		
			$chunk->set('page_start', 12);
			$chunk->set('size', $chunk->page_start + current(unpack('N', $this->read(4))));
			$chunk->set('format', $this->read(4));
			$this->_chunks[] = $chunk;
						
			while(true){

				$chunk = new ChunkMarker($this->tell());
				
				$chunk->set('id', $this->read(4));
			
				switch($chunk->id){
				
					case 'COMM': // sox can place these in an unexpected order
					case 'COMT':
					
						$chunk->set('page_start', 8);
						$chunk->set('size', $chunk->page_start + current(unpack('N', $this->read(4))));
						$chunk->seek_next($this);
						
						break;
						
					case 'SSND':
						$this->_audio_bytes =  current(unpack('N', $this->read(4))) - 8;
						
					default:
						break 2;
				}
				
				$this->_chunks[] = $chunk;
			}
		}
		
		return $this->_chunks;
	}

	protected function _get_chunk_by_id( $CKID )
	{
		foreach($this->_read_chunks() as $chunk)
			if($chunk->id == $CKID)
				return $chunk;
	}

	protected function _read_properties()
	{	
		// big endian
		$short = 'n';
		$long  = 'N';
		
		$comm = $this->_get_chunk_by_id('COMM');
				
		$comm->seek_page($this);
		$comm_page = $this->read( $comm->page_size() );
		
		$upack = $short.'channels/'
				.$long.'samples/'
				.$short.'bit_depth/';
			
		if(!$data = unpack($upack, substr($comm_page, 0, 8)))
			throw new FormatException("The AIFF header at {$this->filename} cannot be unpacked");
		
		// typically a float in Apple SANE format
		$data += array(
			'sample_rate' => static::big2float( substr($comm_page, 8, 10)  ),
			'audio_bytes' => $this->_audio_bytes
		);
		
		return $data;
	}
	
	protected function _read_tags()
	{
		$metadata = array();
		
		if($comt = $this->_get_chunk_by_id('COMT')){
	
			/*
				typedef struct {
				  ID              chunkID;
				  long            chunkSize;
				
				  unsigned short  numComments;
				  char            comments[];
				}CommentsChunk;
			*/
				
			$comt->seek_page( $this );
	
			$metadata = array();
			$numComments = current(unpack('n', $this->read(2)));
			
			for($i=0; $i<$numComments; $i++){
	
				/*
					typedef struct {
					  unsigned long   timeStamp;
					  MarkerID        marker; (short)
					  unsigned short  count;
					  char            text[];
					} Comment;
				*/
	
				$this->seek(6, \SEEK_CUR);
				$count = current(unpack('n', $this->read(2)));
	
				list($key, $val) = explode('=', $this->read($count), 2);
				$metadata[strtoupper($key)] = $val;
			}
		}

		return $metadata;
	}

}