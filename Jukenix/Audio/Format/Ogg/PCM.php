<?php

namespace Jukenix\Audio\Format\Ogg;

use Jukenix\Audio\Format\Ogg;
use Jukenix\Audio\Interfaces\AudioFormat;
use Jukenix\Audio\Definition\AudioFile;
use Jukenix\Audio\Definition\ChunkMarker;
use Jukenix\Audio\Exception\FormatException;

class PCM extends Ogg implements AudioFormat
{
	const ID = 'pcm.oga';
	const COMPRESSED = false;
	const LOSSLESS = true;
	
	public static function identify_resource( AudioFile $BIN )
	{
		return parent::_extract_format_id($BIN) == 'pcm';
	}
	
}
