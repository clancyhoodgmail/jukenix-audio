<?php

namespace Jukenix\Audio\Format\Ogg;

use Jukenix\Audio\Format\Ogg;
use Jukenix\Audio\Interfaces\AudioFormat;
use Jukenix\Audio\Definition\AudioFile;
use Jukenix\Audio\Definition\ChunkMarker;
use Jukenix\Audio\Exception\FormatException;

class Vorbis extends Ogg implements AudioFormat
{
	const ID = 'oga';
	const COMPRESSED = true;
	const LOSSLESS = false;
	
	protected static $encoder_options = array(
		'quality' => 'float; between -1 and 10; step 0.5; default 3',
		'bitrate' => 'int; between 32000 and 1024000; step 32000', // oggenc actually takes kbps
		'bitrate_min' => 'int; between 32000 and 1024000; step 32000',
		'bitrate_max' => 'int; between 32000 and 1024000; step 32000',
		'impulse_noisetune' => 'int; between -15 and 0',
		'lowpass_frequency' => 'int; between 10 and 22500' // oggenc takes khz
	);
	
	public static function identify_resource( AudioFile $BIN )
	{	
		return parent::_extract_format_id($BIN) == 'vorbis';
	}

	protected function _identify_comment_page()
	{
		// we're only interested in the comment block
		if(($this->litread(1) != 0b11) || ($this->read(6) != 'vorbis'))
			throw new FormatException("It isn't legal to omit the comment page in an Ogg Vorbis");
			
		return true;
	}

	protected function _read_properties()
	{	
		/*			
		  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1| Byte
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | packtype      | Identifier char[6]: 'vorbis'                  | 0-3
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                                               | version       | 4-7
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 |                                               | channels      | 8-11
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | rate                                                          | 12-15
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | bitrate_upper                                                 | 16-19
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | bitrate_nominal                                               | 20-23
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | bitrate_lower                                                 | 24-27
		 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		 | l2bs0 | l2bs1 |e|                                               28-31
		 +-+-+-+-+-+-+-+-+-+
		
		*/
		
		$ogg_data = $this->_read_ogg();
		
		$chunk = $ogg_data['bos'];
		$chunk->seek_page( $this );
		$this->seek(7, SEEK_CUR);
		
		$chunk->set('stream_version', $this->read(4));
		
		$data['channels']    = $this->litread(1);
		$data['sample_rate'] = $this->litread(4);
		
		$bitrate_max     = $this->litread(4);
		$bitrate_nom     = $this->litread(4);
		$bitrate_min     = $this->litread(4);
		
		// skip meaning of these
		$blocksize_small = $this->litread(1);
		$blocksize_large = $this->litread(1);
		$stop_bit        = $this->litread(1);
		
		switch(0xFFFFFFFF){
		
			case $bitrate_max:
			case $bitrate_min:
				$data['bitrate_mode'] = Audio::ABR;
				break;
				
			default:
				$data['bitrate_mode'] = Audio::VBR;
		}
		
		if($bitrate_nom != 0xFFFFFFFF)
			$data['options']['bitrate'] = $bitrate_nom;

		if($bitrate_max != 0xFFFFFFFF)
			$data['options']['bitrate_max'] = $bitrate_max;

		if($bitrate_min != 0xFFFFFFFF)
			$data['options']['bitrate_min'] = $bitrate_min;
		
		$chunk = $ogg_data['eos'];
		
		// last chunk
		$data['samples'] = $ogg_data['eos']->granule_position;
		$data['audio_bytes'] = $this->bytes() - $ogg_data['non_audio_bytes'];
		
		return $data;
	}
}
