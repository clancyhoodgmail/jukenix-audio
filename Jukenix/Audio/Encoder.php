<?php

namespace Jukenix\Audio;

abstract class Encoder extends Executable
{
	// Format for which encoder was designed, e.g flac for flac
	const FORMAT_PRIMARY = 0b1000;

	// good transit formats, i.e. uncompressed with headers such as AIFF or WAV
	const FORMAT_TRANSIT = 0b0100;
	
	// accepted input formats
	const FORMAT_INPUT   = 0b0010;
	
	// accepted output formats
	const FORMAT_OUTPUT  = 0b0001;
	
	const OPTION_GOOD   = 0;
	const OPTION_FAIL   = 1;
	const OPTION_STRICT = 2;
	
	protected static $option_grace = 0;	

	/* For use by extensions */

	protected static $binary_path;
	
	protected static $command_pattern;

	protected static $formats = array();
	
	protected $source;
	
	protected $destination;
	
	protected $options;
	
	protected $sections = array();
	
	public function __construct( $source, $destination, array $options, $parent = false )
	{
		$this->source = $source;
		$this->destination = $destination;
		$this->options = $options;
		
		if( $parent )
			$this->fix_parent_options( $parent );
	}
	
	protected function override_option( $key, $value )
	{
		$this->options[ $key ] = $value;
	}
	
	public function option( $key )
	{
		return isset( $this->options[$key] ) ? $this->options[$key] : null;
	}
	
	protected function fix_parent_options( Encoder $parent )
	{
		// 
	}
	
	public function command()
	{		
		$pattern = array(
			'{bin}' => Executable::get_executable( get_called_class() ),
			'{source}' => ($this->source ? escapeshellarg($this->source) : '-'),
			'{destination}' => ($this->destination ? escapeshellarg($this->destination) : '-')
		);
		
		// catch any other placeholders
		preg_match_all('/(\{[a-z\-]+\})/', static::$command_pattern, $matches);
		
		foreach($matches[0] as $match)
			$pattern += array($match => '');
			
		$sections = array();
		
		foreach($this->options as $key => $value){
		
			$setter = "set_option_$key";
		
			if(method_exists($this, $setter))
				$this->$setter($value);
		}
		
		foreach($this->sections as $section => $switches)
			$pattern['{'.$section.'}'] = implode(' ', $switches);
		
		return escapeshellcmd( 
			str_replace( array_keys($pattern), $pattern, static::$command_pattern ) 
		);
	}

	public static function collate_encoders( $format, $byte = Encoder::FORMAT_PRIMARY )
	{
		$candidates = array();
		
		foreach(Executable::executables(get_class()) as $Encoder => $path )
			if($Encoder::format_suitability($format) & $byte)
				$candidates[] = $Encoder;
				
		return $candidates;
	}
	
	public static function match_formats( $input_format, $output_format )
	{		
		foreach(Executable::executables(get_class()) as $Encoder => $path )
			if($Encoder::suit_formats( $input_format, $output_format ))
				return $Encoder;
	}
	
	public static function command_strings( $commands )
	{
		$strings = array();
		
		foreach((array)$commands as $Encoder)
			$strings[] = $Encoder->command();
			
		return $strings;
	}
	
	public static function collate_formats( $byte )
	{	
		$formats = array();
		
		foreach(static::$formats as $format => $fbyte)
			if($fbyte & $byte)
				$formats[] = $format;
		
		return $formats;
	}

	public static function format_suitability( $format )
	{
		return isset(static::$formats[$format]) ? static::$formats[$format] : 0;
	}
	
	public static function suit_formats( $input_format, $ouput_format )
	{
		if(!$input_byte = static::format_suitability($input_format))
			return false;
			
		if(!$output_byte = static::format_suitability($ouput_format))
			return false;
			
		if(($input_byte & Encoder::FORMAT_INPUT) && ($output_byte & Encoder::FORMAT_PRIMARY))
			return true;
			
		if(($input_byte & Encoder::FORMAT_PRIMARY) && ($output_byte & Encoder::FORMAT_OUTPUT))
			return true;
			
		return false;
	}
}