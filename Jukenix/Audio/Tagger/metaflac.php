<?php

namespace Jukenix\Audio\Tagger;

use Jukenix\Audio\Tagger;

abstract class metaflac extends Tagger
{
	
	public static function can_tag_format( $format )
	{
		return ($format == 'flac');
	}
	
	public static function build_command( $filepath, array $tags )
	{
		$strings = array();
		
		foreach($tags as $name => $tag)
			foreach($tag->strings() as $value)
				$strings[] = escapeshellarg(strtoupper($name) . "=$value");
		
		return escapeshellarg($filepath) . ' --remove-all-tags --set-tag=' . implode(' --set-tag=', $strings);
	}

}