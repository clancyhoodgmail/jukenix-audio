<?php

namespace Jukenix\Audio\Tagger;

use Jukenix\Audio\Tagger;

abstract class vorbiscomment extends Tagger
{
	
	public static function can_tag_format( $format )
	{
		$split = explode('.', $format);
		return end( $split ) == 'oga';
	}
	
	public static function build_command( $filepath, array $tags )
	{
		$strings = array();
		
		foreach($tags as $name => $tag)
			foreach($tag->strings() as $value)
				$strings[] = escapeshellarg(strtoupper($name) . "=$value");
		
		$tag_string = '--tag ' . implode(' --tag ', $strings);
		
		return "--write --raw $tag_string ".escapeshellarg($filepath);
	}

}