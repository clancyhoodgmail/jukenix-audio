<?php

namespace Jukenix\Audio;

abstract class Tagger extends Executable
{
	const TAG_OVERWRITE  = 0x00;
	const TAG_MERGE_SOFT = 0x01;
	const TAG_MERGE_HARD = 0x10;

	protected static $formats = array();
	
	public static function tag( Audio $Audio, array $data, $policy = Tagger::TAG_OVERWRITE )
	{
		if( !$Audio->is_taggable() ) // graceful fail
			return false;
			
		if( !$TaggerClass = static::find_tagger( $Audio->get_property('format') ) )
			return false;
		
		switch( $policy ){
		
			case Tagger::TAG_OVERWRITE:
				
				$before_hash = false;
				$Tags = new Tags( $data );
				
			break;
			
			case Tagger::TAG_MERGE_SOFT:
				
				$Tags = clone $Audio->tags();
				$before_hash = $Tags->hash();
				
				foreach($data as $key => $value)
					$Tags->add($key, $value);
				
			break;
			
			case Tagger::TAG_MERGE_HARD:

				$Tags = clone $Audio->tags();
				$before_hash = $Tags->hash();
				
				foreach($data as $key => $value)
					$Tags->set($key, $value);

			break;			
		}
		
		if( $before_hash == $Tags->hash() )
			return true;
	
		$command = Executable::get_executable( $TaggerClass ) . ' ' . $TaggerClass::build_command( $Audio->get_filepath(), $Tags->asArray() );
		
		static::execute($command);
		
		$Audio->tags()->import( $Tags );
			
		return true;
	}
	
	public static function apply_tags( Audio $Audio, Tags $Tags )
	{
		if( !$Audio->is_taggable() ) // graceful fail
			return false;
			
		if( !$TaggerClass = static::find_tagger( $Audio->get_property('format') ) )
			return false;

		$command = Executable::get_executable( $TaggerClass ) . ' ' . $TaggerClass::build_command( $Audio->get_filepath(), $Tags->asArray() );
		
		static::execute($command);
		
		$Audio->tags()->import( $Tags );
			
		return true;
	}
	
	public static function find_tagger( $format )
	{
		foreach(Executable::executables( get_class() ) as $TaggerClass => $executable)
			if($TaggerClass::can_tag_format($format))
				return $TaggerClass;
				
		return false;
	}
	
	public static function can_tag_format( $format )
	{
		return false;
	}
	
	
}