<?php

namespace Jukenix\Audio\Transcoder;

use Jukenix\Audio\Transcoder;
use Jukenix\Exception\TranscoderException;

/*
	flac: free lossless audio codec
	https://xiph.org/flac/documentation_tools_flac.html
*/

abstract class flac extends Transcoder{

	protected static $binary_path = '/usr/bin/flac';
	
	protected static $command_pattern = '{bin} {input-options} {source} {output-options} -o {destination}';

	protected static $formats = array(
		'flac'     => 0b1011, // P-IO
		'flac.oga' => 0b1011, // P-IO
		'pcm'      => 0b0011, // --IO
		'aiff'     => 0b0111, // -TIO
		'wav'      => 0b0011, // --IO
		'w64'      => 0b0111, // -TIO
		'rf64'     => 0b0011, // --IO
	);
	
	protected static function pcm_section($options)
	{
		switch('pcm'){
		
			case $options['input_format']:
				return 'input-options';
				
			case $options['output_format']:
				return 'output-options';
		}
	}
	
	protected static function set_option_signed( $bool, $options, &$sections ){
	
		if($section = static::pcm_section($options))
			$sections[$section][] = '--sign ' . ($bool ? 'signed' : 'unsigned');
	}

	protected static function set_option_endianness( $bool, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = '--endian ' . ($bool ? 'little' : 'big');
	}
	
	protected static function set_option_channels( $int, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = "--channels $int";
	}
	
	protected static function set_option_sample_rate( $int, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = "--sample-rate $int";
	}
	
	protected static function set_option_bit_depth( $int, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = "--bps $int";
	}
	
	protected static function set_option_input_format( $format, $options, &$sections )
	{		
		if(static::$formats[$format] & Transcoder::FORMAT_PRIMARY)
			$sections['input-options'][] = '--decode';
	}
	
	protected static function set_option_output_format( $format, $options, &$sections )
	{		
		switch($format){
		
			case 'pcm':
				$sections['output-options'][] = '--force-raw-format';
				break;
		
			case 'flac.oga':
				$sections['output-options'][] = '--ogg';
				break;
				
			case 'aiff':
				$sections['output-options'][] = '--force-aiff-format';
				break;
				
			case 'w64':
				$sections['output-options'][] = '--force-wave64-format';
				break;

			case 'rf64':
				$sections['output-options'][] = '--force-rf64-format';
				break;
		}
	}

	
}