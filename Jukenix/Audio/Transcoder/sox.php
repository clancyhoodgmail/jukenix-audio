<?php

namespace Jukenix\Audio\Transcoder;

use Jukenix\Audio\Transcoder;
use Jukenix\Exception\TranscoderException;

abstract class sox extends Transcoder{

	protected static $binary_path = '/usr/bin/sox';
	
	protected static $command_pattern = '{bin} {input-options} {source} {output-options} {destination}';

	protected static $formats = array(
		'8svx'      => 0b1011, 
		'aiff'      => 0b1111, 
		'al'        => 0b1011, 
		'amb'       => 0b1011, 
		'au'        => 0b1011, 
		'avr'       => 0b1011, 
		'caf'       => 0b1011, 
		'cdda'      => 0b1011, 
		'cdr'       => 0b1011, 
		'cvs'       => 0b1011, 
		'cvsd'      => 0b1011, 
		'cvu'       => 0b1011, 
		'dat'       => 0b1011, 
		'dvms'      => 0b1011, 
		'f32'       => 0b1011, 
		'f4'        => 0b1011, 
		'f64'       => 0b1011, 
		'f8'        => 0b1011, 
		'fap'       => 0b1011, 
		'fssd'      => 0b1011, 
		'gsm'       => 0b1011, 
		'hcom'      => 0b1011, 
		'htk'       => 0b1011, 
		'ima'       => 0b1011, 
		'ircam'     => 0b1011, 
		'la'        => 0b1011, 
		'lpc'       => 0b1011, 
		'lpc10'     => 0b1011, 
		'lu'        => 0b1011, 
		'm4a'       => 0b1011, 
		'mat'       => 0b1011, 
		'mat4'      => 0b1011, 
		'mat5'      => 0b1011, 
		'maud'      => 0b1011, 
		'mp2'       => 0b1011, 
		'mp3'       => 0b1011, 
		'mp4'       => 0b1011, 
		'nist'      => 0b1011, 
		'paf'       => 0b1011, 
		'pcm'       => 0b1011, 
		'prc'       => 0b1011, 
		'pvf'       => 0b1011, 
		's1'        => 0b1011, 
		's16'       => 0b1011, 
		's2'        => 0b1011, 
		's24'       => 0b1011, 
		's3'        => 0b1011, 
		's32'       => 0b1011, 
		's4'        => 0b1011, 
		's8'        => 0b1011, 
		'sb'        => 0b1011, 
		'sd2'       => 0b1011, 
		'sds'       => 0b1011, 
		'sf'        => 0b1011, 
		'sl'        => 0b1011, 
		'smp'       => 0b1011, 
		'snd'       => 0b1011, 
		'sndfile'   => 0b1011, 
		'sndr'      => 0b1011, 
		'sndt'      => 0b1011, 
		'sou'       => 0b1011, 
		'sox'       => 0b1011, 
		'sph'       => 0b1011, 
		'sw'        => 0b1011, 
		'txw'       => 0b1011, 
		'u1'        => 0b1011, 
		'u16'       => 0b1011, 
		'u2'        => 0b1011, 
		'u24'       => 0b1011, 
		'u3'        => 0b1011, 
		'u32'       => 0b1011, 
		'u4'        => 0b1011, 
		'u8'        => 0b1011, 
		'ub'        => 0b1011, 
		'ul'        => 0b1011,
		'oga'       => 0b0010, // an input format for the purposes of decoding to raw
		'uw'        => 0b1011, 
		'vms'       => 0b1011, 
		'voc'       => 0b1011, 
		'vox'       => 0b1011, 
		'w64'       => 0b1011, 
		'wav'       => 0b1111, 
		'wv'        => 0b1011, 
		'wve'       => 0b1011, 
		'xa'        => 0b1011, 
		'xi'        => 0b1011,
		"adpcm.wav" => 0b1011, 
		"ieee.wav"  => 0b1011, 
		"vselp.wav" => 0b1011, 
		"vcsd.wav"  => 0b1011, 
		"alaw.wav"  => 0b1011, 
		"ulaw.wav"  => 0b1011, 
		"dts.wav"   => 0b1011, 
		"sonarc.wav" => 0b1011, 
		"ac3.wav"   => 0b1011, 
		"aac.wav"   => 0b1011, 
		"wma.wav"   => 0b1011,  
		"ac2.wav"   => 0b1011, 
		"gsm.wav"   => 0b1011,
	);
	
	public static $format_translation = array(
		'wav' => 'wavpcm',
		'pcm' => 'raw',
		'oga' => 'vorbis',
	);

	protected static function pcm_section($options)
	{
		switch('pcm'){
		
			case $options['input_format']:
				return 'input-options';
				
			case $options['output_format']:
				return 'output-options';
		}
	}

	protected static function set_option_signed( $bool, $options, &$sections ){
	
		if($section = static::pcm_section($options))
			$sections[$section][] = '--e ' . ($bool ? 'signed' : 'unsigned');
	}

	protected static function set_option_endianness( $bool, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = ($bool ? '-L' : '-B');
	}
	
	protected static function set_option_channels( $int, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = "-c $int";
	}
	
	protected static function set_option_bit_depth( $int, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = "-b $int";
	}

	protected static function set_option_sample_rate( $int, $options, &$sections ){

		if($section = static::pcm_section($options))
			$sections[$section][] = "--r $int";
	}

	protected static function set_option_output_format( $format, $options, &$sections )
	{		
	
		if(isset(static::$format_translation[$format]))
			$format = static::$format_translation[$format];
				
		$sections['output-options'][] = '-t '.escapeshellarg($format);
	}

}