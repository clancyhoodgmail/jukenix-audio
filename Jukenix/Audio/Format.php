<?php

namespace Jukenix\Audio;

use Jukenix\Audio\Exception\AccessException;
use Jukenix\Audio\Exception\FormatException;

abstract class Format extends AudioFile
{

	const RATE_MAX = false;
	const RATE_MIN = false;
	
	const CHAN_MAX = false;
	const CHAN_MIN = 1;
	
	const DEPTH_MAX = false;
	const DEPTH_MIN = 8;
		
	protected static $encoder_options = array(
		// example  => 'float; between 0.1 and 1.0; step 0.1; def 0.5'
		// example2 => 'int; in 32000 64000 128000; def 64000
		// example3 => 'char; in a b c d; def a
		// example4 => 'bool'
	);

	public static function fix_options( $format, array &$options )
	{
	
		$FormatClass = Format::get_format($format);
		
		foreach(array(
			'RATE' => 'sample_rate',
			'CHAN' => 'channels',
			'DEPTH' => 'bit_depth'
		) as $CONST => $key){
		
			if(isset($options[$key])){
			
				$options[$key] = (int) $options[$key];
				$MIN = constant("$FormatClass::{$CONST}_MIN");
				$MAX = constant("$FormatClass::{$CONST}_MAX");

				if($MIN && ($options[$key] < $MIN)){
					trigger_error("sample_rate option '{$options[$key]}' is less than $format::{$CONST}_MIN", E_USER_NOTICE);
					$value = $MIN;
				}				
				
				else if($MAX && ($options[$key] > $MAX)){
					trigger_error("sample_rate option '{$options[$key]}' is greater than $format::{$CONST}_MAX", E_USER_NOTICE);
					$value = $MAX;
				}
			}
		}
	
		foreach($options as $key => &$value){
				
			if(isset($FormatClass::$encoder_options[$key])){
			
				$parts = array_filter(explode(';', $FormatClass::$encoder_options[$key]));
			
				switch($type = array_shift($parts)){
				
					case 'int':
						$cast = 'intval';
					break;

					case 'float':
						$cast = 'floatval';
					break;
					
					case 'bool':
						$cast = $function($value){
							return (int) (bool) $value;
						};
					break;
					
					case 'char':
						$cast = $function(&$value){
							return (string) $value;
						};
					break;
					
					default:
						throw new FormatException("$FormatClass::encoder_options bad variable type '$type'");						
				}
				
				$default = null;
				$value = $cast($value);
				
				foreach($parts as $part){
				
					$words = array_filter(explode(' ', $part));
					
					switch(array_shift($words)){
					
						case 'between':
						
							$min = $cast(array_shift($words));
							$max = $cast(array_pop($words));
							
							if($value < $min){
								trigger_error("'$key' option '$value' is out of range of $format::encoder_options[$key]", E_USER_NOTICE);
								$value = $min;
							}
							
							else if($value > $max){
								trigger_error("'$key' option '$value' is out of range of $format::encoder_options[$key]", E_USER_NOTICE);
								$value = $max;										
							}
						
						break;
						
						case 'step':
						
							$step = $cast(array_pop($words));
							
							if($value % $step){
								trigger_error("'$key' option '$value' is not in step with $format::encoder_options[$key]", E_USER_NOTICE);
								$value-= ($value % $step);
							}
								
						break;

						case 'in':
							
							foreach($words as &$word)
								$word = $cast($word);
						
							if(!in_array($value, $words)){
								trigger_error("'$key' option '$value' is not within $format::encoder_options[$key]", E_USER_NOTICE);
								
								switch($type){
									
									case 'int':
									case 'float':
									
										$compare = array();
										
										foreach($words as $word)
											$compare[abs($word - $value)] = $word;
											
										ksort($compare);
										$value = array_shift($compare);
									
									break;
									
									default:
										$value = $default;
								}
							}
						
						break;

						case 'default':
						case 'def':
							
							$default = $cast(array_pop($words));
						
							if(is_null($value))
								$value = $default;
	
						break;
					}
				}
				
				if(is_null($value)){
					trigger_error("Failed to set a default option for '$key', having failed $format::encoder_options[$key]", E_USER_NOTICE);
					unset($options[$key])
				}
			}
		}
	}
		
	/*
		Container formats are varied, e.g. Ogg streams are true streams which don't know their length until 
		the last page. Hence we can't define much here, and implementing formats need to read their files
		in their own, most efficent way available.
	*/
	
	protected $_properties;
	
	protected $_tags;

	public function properties()
	{
		if(is_null($this->_properties))
		
			$this->_properties = array(
				'format' => $this::ID,
				'bytes' => $this->bytes(),
			) + $this->_read_properties();
		
		return $this->_properties;
	}

	public function tags()
	{
		if(is_null($this->_tags))
		
			$this->_tags = $this->_read_tags();
		
		return $this->_tags;
	}

	protected function _read_tags()
	{
		return array();
	}
}